module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parser: "babel-eslint",
  extends: [
    'airbnb',
    "plugin:jest/recommended"
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'jest',
  ],
  rules: {
    "semi": ['error', 'never'],
    'no-console': 0,
    'global-require': 0,
    'quote-props' : 0,
    'no-underscore-dangle': 0,
    'import/no-cycle': 0,
    'no-param-reassign': 0,
    'brace-style': ['error', '1tbs'],
    "indent": ["error", 2]
  },
  
};
