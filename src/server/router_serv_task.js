// @flow
import Router from 'koa-router'
import getIdUser from './auth/get_id_user'
import createTask from './tasks_metods/post_task'
import deleteTask from './tasks_metods/delete_task'
import changeTask from './tasks_metods/change_task'
import getTaskAccesUser from './tasks_metods/getTaskAccessUser'
import filterTask from './tasks_metods/filterTask'

function routerTask() {
  const router = new Router({
    prefix: '/task',
  })
  router.get('/accesTask/:id', getTaskAccesUser)
  router.get('/', filterTask)
  router.post('/', createTask)
  router.delete('/:id', deleteTask)
  router.delete('/', async (ctx) => {
    try {
      const { userId } = await getIdUser(ctx)
      await ctx.db.collection('tasks').deleteMany({ userId, status: true })
      ctx.body = await ctx.db.collection('tasks').find({ userId }).toArray()
    } catch (err) {
      console.log(err)
      ctx.status = 500
    }
  })
  router.put('/:id', changeTask)
  return [router.routes(), router.allowedMethods()]
}

export default routerTask
