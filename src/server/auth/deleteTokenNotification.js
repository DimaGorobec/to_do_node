// @flow
import getIdUser from './get_id_user'

const url = 'localhost:27017/test' // Connection URL
const db = require('monk')(url)


const deleteTokenNotification = async (ctx : {
  params: {
    id: string
  },
  request: {
    header: {
    token: string,
    }
  },
  db: {
    collection: Function
  },
  status: number
}) => {
  try {
    // const { token } = ctx.request.header
    const collection = await db.get('notificationToken')
    const { userlogin } = await getIdUser(ctx)
    const some = await collection.findOneAndDelete({ 'userlogin': userlogin })
    console.log('TCL: some', some)
  } catch (err) {
    console.log(err)
    ctx.status = 500
  }
}


export default deleteTokenNotification
