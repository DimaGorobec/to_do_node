import { ObjectID } from 'mongodb'
import getIdUser from './get_id_user'

const sortAccessUsers = async (ctx) => {
  try {
    const { dragIndex, hoverIndex } = ctx.request.body
    const { id } = ctx.params
    const { userId, viewAccess } = await getIdUser(ctx)
    const changedArray = viewAccess.map((element) => {
      if (element.index === dragIndex) {
        element.index = hoverIndex
      }
      if (element.userId.toString() === id) {
        element.index = dragIndex
      }
      return element
    })
    const some = await ctx.db.collection('user')
      .updateOne({ _id: ObjectID(userId) }, { $set: { viewAccess: changedArray } })
    ctx.status = 200
  } catch (error) {
    ctx.status = 500
  }
}
export default sortAccessUsers
