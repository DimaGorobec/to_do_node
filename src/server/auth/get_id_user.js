// @flow
import jwt from 'jsonwebtoken'
import { ObjectID } from 'mongodb'
import parseToken from './helper'

const getIdUser = async (ctx: {
  request: {
    header: {
      token: string
      }
    },
  db: {
    collection: string => Function
    }
  }) => {
  const { token } = ctx.request.header
  const {
    _id,
  } = parseToken(token)
  const dataForToken = {
    _id,
  }
  const newToken: string = await jwt.sign(dataForToken, 'shhhhh')
  const userDataInCollection: {
    _id: string,
    userlogin: string,
    userPassword: string,
    viewAccess: Array<string>
  } = await ctx.db.collection('user').findOne({ '_id': ObjectID(_id) })
  const { viewAccess, userlogin } = userDataInCollection
  const userId: string = userDataInCollection._id
  return {
    userId, newToken, viewAccess, userlogin,
  }
}
export default getIdUser
