// @flow
import getIdUser from './get_id_user'

const ALL_USER = 0
const ACCESS_USER = 1

async function getAllUser(ctx: {
  request: {
    header: {
      token: string
    },
  },
  query: {
    status: string
  },
  db: {
    collection: Function
    },
  body: {
    userId: string,
    userList: Array<{
      userlogin: string,
      userPassword: string
    }>
  },
  status: number
  }) {
  try {
    const { status } = ctx.query
    const { userId, viewAccess } = await getIdUser(ctx)
    let userList = []
    if (+status === ALL_USER) {
      const allUser = await ctx.db.collection('user')
        .find({ _id: { $ne: userId } }).toArray()
      userList = allUser.map((elem) => {
        delete elem.userPassword
        return elem
      })
      ctx.body = { userList, userId }
    } else if (+status === ACCESS_USER) {
      // const accessUser = await ctx.db.collection('user')
      //   .find({ '_id': { $in: viewAccess } }).toArray()
      // userList = accessUser.map((element) => {
      //   const { userlogin, _id } = element
      //   return { userlogin, _id }
      // })
      userList = [...viewAccess]
      userList.sort((a, b) => parseFloat(a.index) - parseFloat(b.index))

      ctx.body = userList
    }
    ctx.status = 200
  } catch (err) {
    console.log(err)
    ctx.status = 404
  }
}

export default getAllUser
