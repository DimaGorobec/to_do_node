// @flow
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const createUser = async (ctx: {
  request: {
    body:{
      userlogin: string,
      userPassword: string,
      viewAccess: Array<string>,
    }
  },
  db: Function,
  status: number,
  body: {
    request: {
      userlogin: string,
      userPassword: string,
      viewAccess: Array<string>,
      _id: string,
      },
      token: string,
  }
}) => {
  try {
    const { userlogin } = ctx.request.body
    const insertToken = {}
    const userData = ctx.request.body
    const saltRounds = 10
    const isUserLogin = await ctx.db.collection('user').findOne({ 'userlogin': userlogin })
    if (!isUserLogin) {
      const salt = await bcrypt.genSaltSync(saltRounds)
      const hash = await bcrypt.hashSync(userData.userPassword, salt)
      userData.userPassword = hash
      userData.viewAccess = []
      const db = await ctx.db.collection('user').insertOne(userData)
      const request = db.ops
      const id = {
        _id: request[0]._id,
      }
      const token = jwt.sign(id, 'shhhhh')
      insertToken.token = token
      await ctx.db.collection('token').insertOne(insertToken)
      ctx.status = 200
      ctx.body = { request, token }
    } else {
      ctx.status = 404
    }
  } catch (err) {
    console.error(err)
    ctx.status = err.statusCode || err.status || 500;
  }
}
export default createUser
