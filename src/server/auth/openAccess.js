// @flow
import { ObjectID } from 'mongodb'
import getIdUser from './get_id_user'
import { io } from '../../../koa'
import sendPushNotification from './pushNotificationOpenAccess'

const openAccessViewing = async (ctx : {
  params: {
    id: string
  },
  request: {
    header: {
    token: string,
    }
  },
  db: {
    collection: Function
  },
  body: Function,
  status: number
}) => {
  try {
    io.sockets.in('room1').emit('message')
    console.log('work io')
    const { userId, userlogin } = await getIdUser(ctx)
    const addedPersonId = ObjectID(ctx.params.id)
    const accessUser = await ctx.db.collection('user').findOne({ _id: ObjectID(addedPersonId) })
    let index = 0
    const userData = {
      userId,
      userlogin,
      index,
    }
    const access = accessUser.viewAccess.some((elem) => {
      if (accessUser.viewAccess.length !== 0) {
        index = elem.index + 1
      }
      return elem.userId.toString() === userId.toString()
    })

    if (!access) {
      ctx.status = 200
      console.log('acces')
      userData.index = index
      accessUser.viewAccess.push(userData)
      sendPushNotification(accessUser.userlogin, userlogin, userId.toString())
      await ctx.db.collection('user').updateOne({ _id: ObjectID(addedPersonId) }, { $set: { viewAccess: accessUser.viewAccess } })
      ctx.body = 1
    } else {
      console.log('denied')
      const removeAccess = accessUser.viewAccess
        .filter((elem) => {
          if (elem.index >= index) {
            elem.index -= 1
          }
          return elem.userId.toString() !== userId.toString()
        })
      await ctx.db.collection('user').updateOne({ _id: ObjectID(addedPersonId) }, { $set: { viewAccess: removeAccess } })
      ctx.body = 0
    }
  } catch (err) {
    console.log(err)
    ctx.status = 500
  }
}


export default openAccessViewing
