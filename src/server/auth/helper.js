// @flow
import jwt from 'jsonwebtoken'

function getDataFromToken(token: string) {
  const { payload: _id } = jwt.decode(token, { complete: true })
  return _id
}

export default getDataFromToken
