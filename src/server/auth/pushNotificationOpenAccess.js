const admin = require('firebase-admin')

const url = 'localhost:27017/test' // Connection URL
const db = require('monk')(url)


async function sendPushNotification(accessUser, login, id) {
  const collection = await db.get('notificationToken')
  const pushNotifcationData = await collection.findOne({ 'userlogin': accessUser })
  if (pushNotifcationData) {
    const { pushNotifToken } = pushNotifcationData
    const message = {
      notification: {
        title: 'Access to Tasks',
        body: `${login} opened access to his tasks`,
      },
      data: {
        userId: id,
      },
    }
    const options = {
      priority: 'high',
      timeToLive: 60 * 60 * 24,
    }
    admin.messaging().sendToDevice(pushNotifToken, message, options)
      .then((response) => {
        console.log('Successfully sent message:', response)
      })
      .catch((error) => {
        console.log('Error sending message:', error)
      })
  }
}

export default sendPushNotification
