// @flow
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const userLogin = async (ctx: {
  request: {
    body: {
      userlogin: string,
      userPassword: string,
      pushNotifToken: string,
      device: string
    }
  },
  status: number,
  db: Function,
  body: {
    token: string,
    status: number,

  }
}) => {
  try {
    const {
      userlogin, userPassword, pushNotifToken, device,
    } = ctx.request.body

    const userData = await ctx.db.collection('user').findOne({ 'userlogin': userlogin })
    if (userData) {
      const user = {
        _id: userData._id,
      }
      const newToken = {
        token: jwt.sign(user, 'shhhhh'),
      }
      await ctx.db.collection('token').insertOne(newToken)
      const accessLogin = bcrypt.compareSync(userPassword, userData.userPassword)
      if (accessLogin) {
        ctx.status = 200
        const response = {
          token: newToken.token,
          status: ctx.status,
        }
        ctx.body = response
      }
      if (pushNotifToken !== null && device !== null) {
        const notifToken = await ctx.db.collection('notificationToken').findOne({ 'pushNotifToken': pushNotifToken })
        if (!notifToken) {
          const newNotificationTokenData = {
            pushNotifToken,
            device,
            userlogin,
          }
          await ctx.db.collection('notificationToken').insertOne(newNotificationTokenData)
        } else {
          await ctx.db.collection('notificationToken')
            .updateOne({ pushNotifToken }, { $set: { userlogin } })
        }
      }
    } else {
      const status: number = 404
      ctx.status = status
    }
  } catch (e) {
    console.log(e)
    ctx.status = 500
  }
}
export default userLogin
