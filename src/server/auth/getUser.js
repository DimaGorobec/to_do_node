// @flow
import { ObjectID } from 'mongodb'
import getDataFromToken from './helper'

const getUserToToken = async (ctx:{
  request: {
    header: {
      token: string
    }
  },
  status: number,
  body: Function,
  db: {
    collection: Function
  },
}) => {
  try {
    const { token } = await ctx.request.header
    const userInfo = getDataFromToken(token)
    const userDataInCollection = await ctx.db.collection('user').findOne({ '_id': ObjectID(userInfo._id) })
    delete userDataInCollection.userPassword
    if (userDataInCollection) {
      ctx.status = 200
      ctx.body = { token, userDataInCollection }
    } else {
      ctx.body = null
    }
  } catch (err) {
    ctx.body = 'null'
    console.log(err)
    ctx.status = err.statusCode || err.status || 500
  }
}
export default getUserToToken
