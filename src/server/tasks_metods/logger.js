// @flow

const logger = (message: string, type: string) => {
  if (process.env.NODE_ENV === 'test') {
    return
  }
  if (type === 'ERROR') {
    console.error(message)
  }
  if (type === 'WARNING') {
    console.warn(message)
  }
  if (type === 'INFO') {
    console.info(message)
  }
}

export default logger
