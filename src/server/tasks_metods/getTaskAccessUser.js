// @flow
import { ObjectID } from 'mongodb'
import getTask from './tasksFromBd'
import collectionUser from './getUser'
import parseToken from './parseToken'
import logger from './logger'


const getTaskAccesUser = async (ctx: {
  params: {
    id: string
  },
  db: Function,
  status: number,
  request: {
    header:{
      token: string
    }
  },
  body: {
    _id: string,
    value: string,
    status: Boolean,
    userId: string
  }
}) => {
  try {
    const collectionTask = await getTask()
    const collectionUsers = await collectionUser()
    const { token } = ctx.request.header
    let idTask
    if (ctx.params.id.length === 24) {
      idTask = ctx.params.id
    }
    const { _id } = parseToken(token)
    const userData = await collectionUsers.findOne({ _id: ObjectID(_id) })

    const some = await collectionUsers.findOne({ _id: ObjectID(idTask) })
    console.log('TCL: some', some.userlogin)
    if (userData) {
      const userTasks = await collectionTask.find({ userId: ObjectID(idTask) })
      ctx.status = userTasks.length !== 0 ? 200 : 404
      ctx.body = userTasks
    } else {
      ctx.status = 404
    }
    return ctx.status
  } catch (err) {
    logger(err, 'ERROR')
    ctx.status = 500
  }
  return ctx.status
}

export default getTaskAccesUser
