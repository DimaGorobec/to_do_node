// @flow
import { ObjectID } from 'mongodb'
import getTask from './tasksFromBd'
import logger from './logger'


const getOneTask = async (ctx: {
  params: {
    id: string
  },
  status: number,
  db: Function,
  body: Function
}) => {
  try {
    const documentQuery = { '_id': ObjectID(ctx.params.id) }
    const collection = await getTask()
    const oneTask = await collection.findOne(documentQuery)
    if (oneTask) {
      ctx.status = 200
    } else {
      ctx.status = 404
    }
    ctx.body = getTask
    return oneTask
  } catch (err) {
    logger(err, 'ERROR')
    ctx.status = 500
  }
}
export default getOneTask
