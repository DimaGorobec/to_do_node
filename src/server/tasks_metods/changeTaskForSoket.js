import { io } from '../../../koa'

function sendChange(id) {
  io.sockets.in('room1').emit('refeshTask', { id })
}

export default sendChange
