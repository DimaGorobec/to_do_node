// @flow
import { ObjectID } from 'mongodb'
import getDataFromToken from '../auth/helper'
import sendChange from './changeTaskForSoket'
import changeTaskPushNotification from './changeTaskPushNotification'


const createTask = async (ctx: {
  request: {
    body: {
      value: string
    },
    header: {
      token: string
    }
  },
  db: Function,
  status: number,
  body: Array<{
    value: string,
    status: Boolean,
    userId: string,
    _id: string
  }>
}) => {
  try {
    const data = ctx.request.body
    const { token: localToken } = ctx.request.header
    const taskData = {
      value: data.value,
      status: false,
      userId: '',
    }
    const userInfo = getDataFromToken(localToken)
    sendChange(userInfo._id)
    const userData = await ctx.db.collection('user')
      .findOne({ '_id': ObjectID(userInfo._id) })
    taskData.userId = userData._id
    const db = await ctx.db.collection('tasks').insertOne(taskData)
    changeTaskPushNotification(userData._id.toString(), 'create a new task')
    ctx.body = db.ops
    ctx.status = 200
  } catch (err) {
    console.error(err)
    ctx.status = err.statusCode || err.status || 500
  }
}

export default createTask
