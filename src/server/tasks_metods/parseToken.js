import jwt from 'jsonwebtoken'

function parseToken(token: string) {
  const { payload: _id } = jwt.decode(token, { complete: true })
  return _id
}

export default parseToken
