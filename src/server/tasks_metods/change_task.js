// @flow
import { ObjectID } from 'mongodb'
import getTask from './tasksFromBd'
import parseToken from './parseToken'
import sendChange from './changeTaskForSoket'
import changeTaskPushNotification from './changeTaskPushNotification'

import logger from './logger'


const CHANGE_STATUS = 1
const CHANGE_TEXT = 0
const changeText = async (ctx: {
  params: {
    id: string
  },
  query: {
    status: string
  },
  request: {
    body: {
      value: string,
      statusTask: string
    },
    header: {
      token: string,
    },
  },
  status: number,
  db: Function,
  body: Array< {
  _id: string,
    value: string,
    status: Boolean,
    userId: string
  }>
}) => {
  try {
    const collection = await getTask()
    let documentQuery
    let _id
    if (ctx.params.id.length === 24) {
      documentQuery = { _id: ObjectID(ctx.params.id) }
      _id = ObjectID(ctx.params.id)
    }
    const { token } = ctx.request.header

    const { _id: userId } = parseToken(token)

    const userTasks = await collection.findOne({ userId: ObjectID(userId), _id: ObjectID(_id) })
    const { status } = ctx.query
    if (parseInt(status, 10) === CHANGE_TEXT && userTasks) {
      const { value } = ctx.request.body
      await collection.findOneAndUpdate(documentQuery, { $set: { value } })
      ctx.status = 200
      await sendChange(userId)
    } else if (parseInt(status, 10) === CHANGE_STATUS && userTasks) {
      const { statusTask } = ctx.request.body
      const changedTask = await collection
        .findOneAndUpdate(documentQuery, { $set: { status: statusTask } })
      await changeTaskPushNotification(userId, 'change status his task')
      ctx.status = 200
      await sendChange(userId)

      ctx.body = changedTask
    } else {
      ctx.status = 404
    }
    ctx.body = await collection.findOne({ _id: userId })
    return ctx.status
  } catch (err) {
    ctx.status = 500
    logger(err, 'ERROR')
    return ctx.status
  }
}

export default changeText
