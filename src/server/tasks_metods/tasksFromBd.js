// const url = '127.0.0.1:27017/test' // Connection URL
const getTask = async () => {
  const db = require('monk')(process.env.LINK_DB)
  const collection = await db.get('tasks')
  console.log('TCL: getTask -> collection', typeof process.env.LINK_DB)
  return collection
}

export default getTask
