import changeTask from '../change_task'
import getOneTask from '../getOneTask'

it('succes change status task', async () => {
  const ctx = {
    params: {
      'id': '5da089cfaf9c7e2f00f9d603',
    },
    query: {
      status: '1',
    },
    request: {
      body: {
        statuTask: false,
      },
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzA4MDM4NDN9.nJBNRJmCFO8pfUllMNctVpru_HNMp_7FfFRJknEptUE',
      },
    },
  }
  const result = await changeTask(ctx)
  expect(result).toBe(200)
})
it('change status task with invalid id', async () => {
  const ctx = {
    params: {
      'id': 'some invalid',
    },
    query: {
      status: '1',
    },
    request: {
      body: {
        statuTask: false,
      },
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzA4MDM4NDN9.nJBNRJmCFO8pfUllMNctVpru_HNMp_7FfFRJknEptUE',
      },
    },
  }
  const result = await changeTask(ctx)
  expect(result).toBe(404)
})
it('change status task other user', async () => {
  const ctx = {
    params: {
      'id': '5da089baaf9c7e2f00f9d600',
    },
    query: {
      status: '1',
    },
    request: {
      body: {
        statuTask: false,
      },
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzA4MDQwODF9.WJKRzJJSFrlfQUgVYKxQ27_dD9N3K_cVl-Lnwid10rE',
      },
    },
  }
  const result = await changeTask(ctx)
  expect(result).toBe(404)
})
