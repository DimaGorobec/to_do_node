import filterTask from '../filterTask'

it('filter task: active tasks successfully', async () => {
  const ctx = {
    query: {
      status: '1',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzExMjgwODN9.a1J1RZYqqqfDR-18e-VL3yQmmd8mqT0TgY-hFxgtHV0',
      },
    },
  }
  const result = await filterTask(ctx)
  expect(result).toBe(200)
})

it('filter task: complited tasks successfully', async () => {
  const ctx = {
    query: {
      status: '0',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzExMjgwODN9.a1J1RZYqqqfDR-18e-VL3yQmmd8mqT0TgY-hFxgtHV0',
      },
    },
  }
  const result = await filterTask(ctx)
  expect(result).toBe(200)
})
it('filter task: all tasks successfully', async () => {
  const ctx = {
    query: {
      status: '2',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzExMjgwODN9.a1J1RZYqqqfDR-18e-VL3yQmmd8mqT0TgY-hFxgtHV0',
      },
    },
  }
  const result = await filterTask(ctx)
  expect(result).toBe(200)
})
it('filter task with invalid token', async () => {
  const ctx = {
    query: {
      status: '2',
    },
    request: {
      header: {
        token: 'eyJhbGciOi23UzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODliNGFmOWM3ZTJmMDBmOWQ1ZmUiLCJpYXQiOjE1NzExMjM1MDV9.txNKq1twu4Npahw-yr5AwNdenJAOguSzh_V8bJ5r9ao',
      },
    },
  }
  const result = await filterTask(ctx)
  expect(result).toBe(500)
})

it('filter task with other status', async () => {
  const ctx = {
    query: {
      status: '3',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODljOGFmOWM3ZTJmMDBmOWQ2MDEiLCJpYXQiOjE1NzExMjgwODN9.a1J1RZYqqqfDR-18e-VL3yQmmd8mqT0TgY-hFxgtHV0',
      },
    },
  }
  const result = await filterTask(ctx)
  expect(result).toBe(404)
})
