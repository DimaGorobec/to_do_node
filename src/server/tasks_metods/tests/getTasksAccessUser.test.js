import getTaskAccesUser from '../getTaskAccessUser'

it('tasks access user done', async () => {
  const ctx = {
    params: {
      'id': '5da089c8af9c7e2f00f9d601',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODliNGFmOWM3ZTJmMDBmOWQ1ZmUiLCJpYXQiOjE1NzExMjM1MDV9.txNKq1twu4Npahw-yr5AwNdenJAOguSzh_V8bJ5r9ao',
      },
    },
  }
  const result = await getTaskAccesUser(ctx)
  expect(result).toBe(200)
})

it('tasks access users with invalid token', async () => {
  const ctx = {
    params: {
      'id': '5da089c8af9c7e2f00f9d601',
    },
    request: {
      header: {
        token: 'eyJhbGciOi23UzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODliNGFmOWM3ZTJmMDBmOWQ1ZmUiLCJpYXQiOjE1NzExMjM1MDV9.txNKq1twu4Npahw-yr5AwNdenJAOguSzh_V8bJ5r9ao',
      },
    },
  }
  const result = await getTaskAccesUser(ctx)
  expect(result).toBe(500)
})

it('tasks access unknown user', async () => {
  const ctx = {
    params: {
      'id': '5da089c8af9c7e2f32f4d601',
    },
    request: {
      header: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwODliNGFmOWM3ZTJmMDBmOWQ1ZmUiLCJpYXQiOjE1NzExMjM1MDV9.txNKq1twu4Npahw-yr5AwNdenJAOguSzh_V8bJ5r9ao',
      },
    },
  }
  const result = await getTaskAccesUser(ctx)
  expect(result).toBe(404)
})
