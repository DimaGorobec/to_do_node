import deleteTask from '../delete_task'
import getOneTask from '../getOneTask'

it('success delete task', async () => {
  const ctx = {
    params: {
      id: '5da5a6738e9d2b37f162ac3b',
    },
  }
  const result = await deleteTask(ctx)
  expect(result).toBe(200)
  expect(await getOneTask(ctx)).toBe(null)
})
it('deleted task with invalid params', async () => {
  const ctx = {
    params: {
      id: 'invalidparams',
    },
  }
  const result = await deleteTask(ctx)
  expect(result).toBe(404)
})
it('deleted task which is already deleted', async () => {
  const ctx = {
    params: {
      id: '5da05848f9afb427183af9a4',
    },
  }
  const result = await deleteTask(ctx)
  expect(result).toBe(404)
})
