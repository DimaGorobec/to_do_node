// @flow

const getTaskAccesUser = async (ctx: {
  params: {
    id: string
  },
  db: Function,
  status: number,
  body: {
    _id: string,
    value: string,
    status: Boolean,
    userId: string
  }
}) => {
  try {
    const idTask = ctx.params.id
    const userTasks = ctx.db.collection.filter((elem) => elem.userId === idTask)
    ctx.status = 200
    ctx.body = userTasks
    return ctx.status
  } catch (err) {
    console.log(err)
    ctx.status = 500
    return ctx.status
  }
}

export default getTaskAccesUser
