// @flow
import { ObjectID } from 'mongodb'
import getTask from './tasksFromBd'
import parseToken from './parseToken'
import sendChange from './changeTaskForSoket'

import logger from './logger'


const deleteTask = async (ctx: {
  params: {
    id: string
  },
  request: {
    header: {
      token: string
    }
  },
  status: number,
  db: Function,
  body: Function,
  logger: Function
}) => {
  try {
    const collection = await getTask()
    let documentQuery
    let deletedTask
    if (ctx.params.id.length === 24) {
      documentQuery = { _id: ObjectID(ctx.params.id) }
      const { token } = ctx.request.header
      const { _id: userId } = parseToken(token)
      sendChange(userId)
      deletedTask = await collection.remove(documentQuery)
      if (deletedTask.result.n === 0) {
        ctx.status = 404
      } else {
        ctx.status = 200
      }
    } else {
      documentQuery = { _id: ctx.params.id }
      ctx.status = 404
    }
    ctx.body = deletedTask
  } catch (err) {
    logger(err, 'ERROR')
    ctx.status = 500
  }
  return ctx.status
}

export default deleteTask
