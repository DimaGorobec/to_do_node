// @flow
import jwt from 'jsonwebtoken'
import { ObjectID } from 'mongodb'
import logger from './logger'

// import getIdUser from '../auth/get_id_user'
import getTask from './tasksFromBd'
import parseToken from './parseToken'

const COMPLITED_TASKS = 0
const ACTIVE_TASKS = 1
const ALL_TASKS = 2

const createTask = async (ctx: {
  query: {
    status: string,
  },
  db: Function,
  request: {
    header: {
      token: string,
    }
  },
  status: number,
  body: {
    userTasks: {
      _id: string,
      value: string,
      status: Boolean,
      userId: string
    },
    newToken: string
  }
}) => {
  try {
    const collectionTask = await getTask()
    const { token } = ctx.request.header
    const { _id } = parseToken(token)
    const newToken: string = await jwt.sign(_id, 'shhhhh')
    const status = parseInt(ctx.query.status, 10)
    if (status === ALL_TASKS) {
      const userTasks = await collectionTask.find({ userId: ObjectID(_id) })
      ctx.body = { userTasks, newToken }
      ctx.status = 200
    } else if (status === ACTIVE_TASKS || status === COMPLITED_TASKS) {
      const userTasks = await collectionTask
        .find({ status: Boolean(status), userId: ObjectID(_id) })
      ctx.body = { userTasks, newToken }
      ctx.status = 200
    } else {
      ctx.status = 404
    }
    return ctx.status
  } catch (err) {
    logger(err, 'ERROR')
    ctx.status = err.statusCode || err.status || 500
  }
  return ctx.status
}

export default createTask
