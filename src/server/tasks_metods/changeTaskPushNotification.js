import { ObjectID } from 'mongodb'

const admin = require('firebase-admin')


const url = 'localhost:27017/test' // Connection URL
const db = require('monk')(url)


async function changeTaskPushNotification(id, textNotification) {
  const collectionTokens = await db.get('notificationToken')
  const collectionUser = await db.get('user')
  const { userlogin } = await collectionUser.findOne({ '_id': ObjectID(id) })

  const allUsers = await collectionUser.find()

  const accessUsers = allUsers.filter((element) => element.viewAccess
    .some((elem) => elem.userlogin === userlogin))

  Promise.all(accessUsers.map(async (element) => {
    const pushNotifcationData = await collectionTokens.findOne({ 'userlogin': element.userlogin })
    if (pushNotifcationData) {
      const { pushNotifToken } = pushNotifcationData
      const message = {
        notification: {
          title: 'Access to Tasks',
          body: `${userlogin} ${textNotification}`,
        },
        data: {
          userId: id,
        },
      }
      const options = {
        priority: 'high',
        timeToLive: 60 * 60 * 24,
        some: 'dasdasd',
      }
      admin.messaging().sendToDevice(pushNotifToken, message, options)
        .then((response) => {
          console.log('Successfully sent message:', response)
        })
        .catch((error) => {
          console.log('Error sending message:', error)
        })
    }
    return element
  }))
}

export default changeTaskPushNotification
