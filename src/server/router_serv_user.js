// @flow
import Router from 'koa-router'
import getUsers from './auth/getAllUSer'
import getUserToToken from './auth/getUser'
import createUsers from './auth/register'
import userLogin from './auth/userLogin_server'
import openAccessViewing from './auth/openAccess'
import sortAccessUsers from './auth/sortAccessUsers'
import deleteTokenNotification from './auth/deleteTokenNotification'

function routerUser() {
  const router = new Router({
    prefix: '/user',
  })

  router.post('/', createUsers)
  router.get('/', getUsers)
  router.get('/refreshToken', getUserToToken)
  router.post('/login', userLogin)
  router.patch('/access/:id', openAccessViewing)
  router.put('/sortAccessUser/:id', sortAccessUsers)
  router.put('/deleteToken', deleteTokenNotification)

  return [router.routes(), router.allowedMethods()]
}

export default routerUser
