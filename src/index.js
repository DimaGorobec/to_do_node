import './scss/main.scss'
import { backAuthorization } from './backAuthorization'
import User from './server/user/authUser'
import dispatch from './actions/globalAction'


export const user = new User()
export const userTask = new Task()
async function isAuthoUser() {
  await user.refreshToken()
  if (!user.isAutirozation) {
    backAuthorization()
  }
}
isAuthoUser()

const btnFilterbox = document.querySelector('.box_filter')

btnFilterbox.addEventListener('click', (e) => {
  const event = e.target
  const allTasks = document.querySelectorAll('.task')
  switch (event.className) {
  case 'show_all':
    dispatch({
      event: 'showAllTasks',
      payload: { event, allTasks },
    })
    break
  case 'show_active':
    dispatch({
      event: 'showActiveTasks',
      payload: { event, allTasks },
    })
    break
  case 'show_complited':
    dispatch({
      event: 'showComplitedTasks',
      payload: { event, allTasks },
    })
    break
  default:
    /* code */
    break
  }
  return e
})

const listUl = document.querySelector('.conteiner-task')
const btnDeleteAllComplited = document.querySelector('.remove_compited')

btnDeleteAllComplited.addEventListener('click', (e) => {
  e.preventDefault()
  userTask.deleteComplitedTask()
})

const writeToDo = document.querySelector('.do_li')
writeToDo.addEventListener('keydown', (event) => {
  if (event.key === 'Enter') {
    const valueTask = writeToDo.value
    if (valueTask.length >= 3) {
      dispatch({
        event: 'createTask',
        payload: { valueTask },
      })
      writeToDo.value = ''
    } else {
      return false
    }
  }
  return event
})

listUl.addEventListener('click', async (e) => {
  const event = e.target
  const { target } = e

  const indexTask = event.parentElement.parentElement.getAttribute('data-key')
  switch (event.className) {
  case 'delete-li':
    dispatch({
      event: 'deleteTask',
      payload: { target, indexTask },
    })
    break
  case 'show_info': {
    dispatch({
      event: 'showModalInfoTask',
      payload: indexTask,
    })
    break
  }
  case 'checked_done':
    dispatch({
      event: 'changeStatusTask',
      payload: { target, indexTask },
    })
    break
  case 'fas fa-save': {
    dispatch({
      event: 'changeTextTask',
      payload: { target },
    })
    break
  }
  case 'fas fa-trash-alt': {
    dispatch({
      event: 'cancelChangeTextTask',
      payload: { target },
    })
    break
  }
  default:
    /* code */
    break
  }
})

listUl.addEventListener('dblclick', (e) => {
  const textNode = e.target
  if (e.target.classList.contains('text_task')) {
    dispatch({
      event: 'openChangeText',
      payload: { textNode },
    })
  }
})
const header = document.querySelector('header')

header.addEventListener('click', (e) => {
  const element = e.target
  // eslint-disable-next-line default-case
  switch (element.className) {
  case 'login':
    dispatch({
      event: 'openLogin',
    })
    break
  case 'open_access':
    dispatch({
      event: 'openAllUsers',
    })
    break
  case 'custom-select show': {
    dispatch({
      event: 'getAllAccessUser',
    })
    break
  }
  case 'logOut_box show': {
    dispatch({
      event: 'logOut',
    })
    break
  }
  case 'registr': {
    dispatch({
      event: 'openRegister',
    })
  }
  }
})
const wrapperLogin = document.querySelector('.wrapper_login')
const wrapperRegistr = document.querySelector('.wrapper_registr')
const closeModalWrapper = document.querySelector('body')

closeModalWrapper.addEventListener('click', (e) => {
  const element = e.target
  // eslint-disable-next-line default-case
  switch (element.className) {
  case 'fas fa-times close_login': {
    wrapperLogin.classList.remove('show')
    const userLoginVal = document.querySelector('.login_userName')
    const userSassVal = document.querySelector('.login_password')
    userLoginVal.value = ''
    userSassVal.value = ''
    break
  }
  case 'delete_modal': {
    const modalDel = document.querySelector('.modal_wrapper')
    modalDel.remove()
    break
  }
  case 'modal_wrapper': {
    const modalDel = document.querySelector('.modal_wrapper')
    modalDel.remove()
    break
  }
  case 'fas fa-times close_registr': {
    wrapperRegistr.classList.remove('active')
    const userLoginVal = document.querySelector('.create_userName')
    userLoginVal.value = ''
    const userSassVal = document.querySelector('.create_password')
    userSassVal.value = ''
    break
  }
  case 'fas fa-times close_allUser': {
    const wrapperAllUser = document.querySelector('.wrpaer_openAccess')
    const allUser = document.querySelectorAll('.user_list li')
    allUser.forEach((elem) => elem.remove())
    wrapperAllUser.classList.remove('active')
    break
  }
  case 'fas fa-times close_accessPage': {
    document.querySelector('.wrapper_accessUser').remove()
    break
  }
  case 'user': {
    const userId = e.target.getAttribute('user-id')
    userTask.renderAccessTasks(userId)
    // getTaskUser(userId)
    break
  }
  }
})

const goToRegistr = document.querySelector('.registr_btn')
goToRegistr.addEventListener('click', async (e) => {
  e.preventDefault()
  dispatch({
    event: 'registerUser',
  })
})
const goToLogin = document.querySelector('.login_btn')
goToLogin.addEventListener('click', async (e) => {
  e.preventDefault()
  dispatch({
    event: 'loginUser',
  })
})
const listAllUser = document.querySelector('.user_list')
listAllUser.addEventListener('click', (e) => {
  if (e.target.classList.contains('register_user')) {
    const idUSer = e.target.getAttribute('user-id')
    const nodeLI = e.target
    dispatch({
      event: 'postAccess',
      payload: { idUSer, nodeLI },
    })
  }
})
