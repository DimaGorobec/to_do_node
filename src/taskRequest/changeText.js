async function changeTextOnServer(value, _id) {
  const task = {
    // eslint-disable-next-line key-spacing
    value,
  }
  const status = 0
  const response = await fetch(`http://localhost:3000/task/${_id}/?status=${status}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(task),
  });
  // eslint-disable-next-line no-unused-vars
  const result = await response.json()
  return result[0]
}
export default changeTextOnServer
