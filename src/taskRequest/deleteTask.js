const serverDelTask = async (value) => {
  const task = {
    _id: value,
  }
  await fetch(`http://localhost:3000/task/${value}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(task),
  });
}
export default serverDelTask
