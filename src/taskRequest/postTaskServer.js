const postTaskServer = async (value) => {
  const task = {
    status: false,
    value,
    token: sessionStorage.tokenData,
  }
  const response = await fetch('http://localhost:3000/task/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(task),
  });
  const result = await response.json();
  return result[0]
}
export default postTaskServer
