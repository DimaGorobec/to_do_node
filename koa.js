import BodyParser from 'koa-bodyparser'
import Koa from 'koa'
import mongo from 'koa-mongo'
import cors from '@koa/cors'
import routerTask from './src/server/router_serv_task'
import routerUser from './src/server/router_serv_user'

require('dotenv').config()

const app = new Koa()
const admin = require('firebase-admin')

const serviceAccount = require('./src/server/auth/serviceAccountKey.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://applica-5e8d9.firebaseio.com',
})
app.use(BodyParser())
app.use(cors())
app.use(mongo())
app.use(...routerTask())
app.use(...routerUser())
const port = process.env.PORT
const server = app.listen(port)
export const io = require('socket.io').listen(server)


io.sockets.on('connection', (socket) => {
  socket.on('create', (room) => {
    socket.join(room)
  })
})
