import { MongoClient } from 'mongodb'

const MONGO_URL = 'mongodb://localhost:3000'
export default function () {
  MongoClient.connect(MONGO_URL, { useNewUrlParser: true })
    .then(() => {
    })
    .catch((err) => console.error(err))
}
