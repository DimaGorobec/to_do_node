'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ObjectID = require('mongodb').ObjectID.ObjectID;

var getId = require('./get_id_user');

exports.openAccessViewing = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var _ref2, userId, addedPersonId, accessUser, access, removeAccess;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return getId.getIdUser(ctx);

          case 3:
            _ref2 = _context.sent;
            userId = _ref2.userId;
            addedPersonId = ctx.request.body.id;
            _context.next = 8;
            return ctx.db.collection('user').findOne({ _id: ObjectID(addedPersonId) });

          case 8:
            accessUser = _context.sent;
            access = accessUser.viewAccess.some(function (elem) {
              return elem.toString() === userId.toString();
            });

            if (access) {
              _context.next = 17;
              break;
            }

            ctx.status = 200;
            accessUser.viewAccess.push(userId);
            _context.next = 15;
            return ctx.db.collection('user').updateOne({ _id: ObjectID(addedPersonId) }, { $set: { viewAccess: accessUser.viewAccess } });

          case 15:
            _context.next = 20;
            break;

          case 17:
            // eslint-disable-next-line max-len
            removeAccess = accessUser.viewAccess.filter(function (elem) {
              return elem.toString() !== userId.toString();
            });
            _context.next = 20;
            return ctx.db.collection('user').updateOne({ _id: ObjectID(addedPersonId) }, { $set: { viewAccess: removeAccess } });

          case 20:
            ctx.body = ctx.request.body;
            _context.next = 26;
            break;

          case 23:
            _context.prev = 23;
            _context.t0 = _context['catch'](0);

            console.log(_context.t0);

          case 26:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 23]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();