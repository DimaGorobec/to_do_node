'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// const { ObjectID } = require('mongodb').ObjectID;
var jwt = require('jsonwebtoken');

exports.createUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var userlogin, token, insertToken, userData, isUserLogin, db, request;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            userlogin = ctx.request.body.userlogin;
            token = jwt.sign(ctx.request.body, 'shhhhh');
            insertToken = {
              'token': token
            };
            _context.next = 6;
            return ctx.db.collection('token').insertOne(insertToken);

          case 6:
            userData = ctx.request.body;

            userData.viewAccess = [];
            // const AllUsers = await ctx.db.collection('user').find().toArray();
            _context.next = 10;
            return ctx.db.collection('user').findOne({ 'userlogin': userlogin });

          case 10:
            isUserLogin = _context.sent;

            if (!(isUserLogin === null)) {
              _context.next = 20;
              break;
            }

            _context.next = 14;
            return ctx.db.collection('user').insertOne(userData);

          case 14:
            db = _context.sent;
            request = db.ops;

            ctx.status = 200;
            ctx.body = { request: request, token: token };
            _context.next = 21;
            break;

          case 20:
            ctx.status = 404;
            // ctx.body = false

          case 21:
            _context.next = 27;
            break;

          case 23:
            _context.prev = 23;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);
            ctx.status = _context.t0.statusCode || _context.t0.status || 500;

          case 27:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 23]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();