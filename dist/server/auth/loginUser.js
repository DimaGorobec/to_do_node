'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginUser = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var loginUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var userNameValue, userPasswordValue, userData, response, result;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            userNameValue = userNameInput.value;
            userPasswordValue = userPasswordInput.value;
            userData = { userNameValue: userNameValue, userPasswordValue: userPasswordValue };
            _context.next = 5;
            return fetch('http://localhost:3000/task/login/', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              body: (0, _stringify2.default)(userData)
            });

          case 5:
            response = _context.sent;
            _context.next = 8;
            return response.json();

          case 8:
            result = _context.sent;

            if (result.status === 200) {
              sessionStorage.tokenData = result.token;
              userNameInput.value = '';
              userPasswordInput.value = '';
              (0, _is_login.isLogin)();
            } else {
              alert('user not registr');
            }

          case 10:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function loginUser() {
    return _ref.apply(this, arguments);
  };
}();
// eslint-disable-next-line import/prefer-default-export


var _is_login = require('../../is_login');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userNameInput = document.querySelector('.login_userName');
var userPasswordInput = document.querySelector('.login_password');
exports.loginUser = loginUser;