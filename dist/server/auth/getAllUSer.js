'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

// const getId = require('./get_id_user');

var getAllUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var _ref2, userId, allUser, userList;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _get_id_user2.default.getIdUser(ctx);

          case 3:
            _ref2 = _context.sent;
            userId = _ref2.userId;
            _context.next = 7;
            return ctx.db.collection('user').find().toArray();

          case 7:
            allUser = _context.sent;


            // console.log(userOnline)
            userList = allUser.filter(function (elem) {
              // eslint-disable-next-line no-param-reassign
              elem.userPassword = null;
              return elem._id.toString() !== userId.toString();
            });

            ctx.body = { userList: userList, userId: userId };
            _context.next = 16;
            break;

          case 12:
            _context.prev = 12;
            _context.t0 = _context['catch'](0);

            console.log(_context.t0);
            ctx.status = 404;

          case 16:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 12]]);
  }));

  return function getAllUser(_x) {
    return _ref.apply(this, arguments);
  };
}(); // const { ObjectID } = require('mongodb').ObjectID;


var _get_id_user = require('./get_id_user');

var _get_id_user2 = _interopRequireDefault(_get_id_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAllUser;