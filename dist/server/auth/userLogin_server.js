'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var jwt = require('jsonwebtoken');

exports.userLogin = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var userlogin, userPassword, userData, user, newToken, response;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            userlogin = ctx.request.body.userNameValue;
            userPassword = ctx.request.body.userPasswordValue;
            _context.next = 5;
            return ctx.db.collection('user').findOne({ 'userlogin': userlogin, 'userPassword': userPassword });

          case 5:
            userData = _context.sent;

            if (!userData) {
              _context.next = 16;
              break;
            }

            user = {
              userlogin: userlogin,
              userPassword: userPassword
            };
            newToken = {
              token: jwt.sign(user, 'shhhhh')
            };
            _context.next = 11;
            return ctx.db.collection('token').insertOne(newToken);

          case 11:
            ctx.status = 200;
            response = {
              token: newToken.token,
              status: ctx.status
            };

            ctx.body = response;
            _context.next = 18;
            break;

          case 16:
            ctx.status = 404;
            ctx.body = ctx.status;

          case 18:
            _context.next = 24;
            break;

          case 20:
            _context.prev = 20;
            _context.t0 = _context['catch'](0);

            console.log(_context.t0);
            ctx.status = 500;

          case 24:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 20]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();