'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var getUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(token) {
    var lockaToken, response, result;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            lockaToken = {
              nameToken: token
            };
            _context.next = 3;
            return fetch('http://localhost:3000/task/searchUser/', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              body: (0, _stringify2.default)(lockaToken)
            });

          case 3:
            response = _context.sent;
            _context.next = 6;
            return response.text();

          case 6:
            result = _context.sent;

            if (result !== false) {
              (0, _is_login.isLogin)();
            } else {
              (0, _backAuthorization.backAuthorization)();
            }
            _context.next = 10;
            return result;

          case 10:
            sessionStorage.tokenData = _context.sent;

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function getUser(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _backAuthorization = require('../../backAuthorization');

var _is_login = require('../../is_login');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getUser;