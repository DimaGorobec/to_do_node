'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getDataFromToken(token) {
  var _jwt$decode = _jsonwebtoken2.default.decode(token, { complete: true }),
      userData = _jwt$decode.payload;

  return userData;
}

exports.default = getDataFromToken;