'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getId = require('./get_id_user');

exports.getAccesUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var _ref2, userId, _ref3, viewAccess;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return getId.getIdUser(ctx);

          case 3:
            _ref2 = _context.sent;
            userId = _ref2.userId;
            _context.next = 7;
            return ctx.db.collection('user').findOne({ '_id': userId });

          case 7:
            _ref3 = _context.sent;
            viewAccess = _ref3.viewAccess;

            viewAccess.forEach(function (element) {});
            ctx.body = ctx.request.body;
            _context.next = 17;
            break;

          case 13:
            _context.prev = 13;
            _context.t0 = _context['catch'](0);

            console.log(_context.t0);
            ctx.status = 500;

          case 17:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 13]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();