'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

// const jwt = require('jsonwebtoken')
// const parseToken = require('./helper')

var getIdUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var localToken, userInfo, dataForToken, newToken, userDataInCollection, userId;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            localToken = ctx.request.body.token;
            userInfo = (0, _helper2.default)(localToken);
            dataForToken = {
              userlogin: userInfo.userlogin,
              userPassword: userInfo.userPassword
            };
            _context.next = 5;
            return _jsonwebtoken2.default.sign(dataForToken, 'shhhhh');

          case 5:
            newToken = _context.sent;
            _context.next = 8;
            return ctx.db.collection('user').findOne({ 'userlogin': userInfo.userlogin, 'userPassword': userInfo.userPassword });

          case 8:
            userDataInCollection = _context.sent;
            userId = userDataInCollection._id;
            return _context.abrupt('return', { userId: userId, newToken: newToken });

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function getIdUser(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _helper = require('./helper');

var _helper2 = _interopRequireDefault(_helper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getIdUser;