'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userData = require('./helper');

exports.getUserToToken = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var _ref2, localToken, userInfo, userDataInCollection;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return ctx.request.body;

          case 3:
            _ref2 = _context.sent;
            localToken = _ref2.nameToken;
            userInfo = userData.getDataFromToken(localToken);
            _context.next = 8;
            return ctx.db.collection('user').findOne({ 'userlogin': userInfo.userName });

          case 8:
            userDataInCollection = _context.sent;

            if (userDataInCollection) {
              ctx.status = 200;
              ctx.body = localToken;
            } else {
              ctx.body = localToken;
            }
            _context.next = 16;
            break;

          case 12:
            _context.prev = 12;
            _context.t0 = _context['catch'](0);

            console.log(_context.t0);
            ctx.status = _context.t0.statusCode || _context.t0.status || 500;

          case 16:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 12]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();