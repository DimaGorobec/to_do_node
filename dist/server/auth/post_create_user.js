'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var createUser = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var userlogin, userPassword, data, userData, response, result;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            userlogin = userNameInput.value;
            userPassword = userPasswordInput.value;
            data = '';

            if (!(userlogin.length > 5 && userPassword.length > 5)) {
              _context.next = 15;
              break;
            }

            userData = { userlogin: userlogin, userPassword: userPassword };
            _context.next = 7;
            return fetch('http://localhost:3000/task/create_user/', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              body: (0, _stringify2.default)(userData)
            });

          case 7:
            response = _context.sent;
            _context.next = 10;
            return response.json();

          case 10:
            result = _context.sent;

            data = result;
            if (data) {
              saveToken(data.token);
              registrBox.classList.add('hide');
              doneRegister.classList.add('show');
              userPasswordInput.value = '';
              userNameInput.value = '';
              setTimeout(function () {
                wrapperRegistr.classList.remove('active');
                conteinerToDo.classList.add('accessTrue');
                registrBox.classList.remove('hide');
                doneRegister.classList.remove('show');
                logoutBox.classList.add('show');
              }, 2000);
            }
            _context.next = 17;
            break;

          case 15:
            userNameInput.classList.add('err');
            userPasswordInput.classList.add('err');

          case 17:
            return _context.abrupt('return', data);

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function createUser() {
    return _ref.apply(this, arguments);
  };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var userNameInput = document.querySelector('.create_userName');
var userPasswordInput = document.querySelector('.create_password');
var registrBox = document.querySelector('.registr_form');
var doneRegister = document.querySelector('.done_register_modal');
var wrapperRegistr = document.querySelector('.wrapper_registr');
var logoutBox = document.querySelector('.logOut_box');
var conteinerToDo = document.querySelector('.conteiner-To_do');
function saveToken(token) {
  sessionStorage.setItem('tokenData', token);
}
exports.default = createUser;