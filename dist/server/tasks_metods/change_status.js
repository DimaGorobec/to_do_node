'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ObjectID = require('mongodb').ObjectID.ObjectID;

exports.changeStatus = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var documentQuery, valuesToUpdate;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            documentQuery = { '_id': ObjectID(ctx.params.id) };
            valuesToUpdate = ctx.request.body.status;
            _context.next = 5;
            return ctx.db.collection('tasks').updateOne(documentQuery, { $set: { status: valuesToUpdate } });

          case 5:
            ctx.body = _context.sent;
            _context.next = 12;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context['catch'](0);

            console.error('error in router post people');
            ctx.status = _context.t0.statusCode || _context.t0.status || 500;

          case 12:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 8]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();