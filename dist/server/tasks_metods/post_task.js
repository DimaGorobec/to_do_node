'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var parseToken = require('../auth/helper');

exports.configRouter = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(ctx) {
    var data, localToken, taskData, userInfo, userData, db;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            data = ctx.request.body;
            localToken = data.token;
            taskData = {
              value: data.value,
              status: data.status
            };
            userInfo = parseToken.getDataFromToken(localToken);
            _context.next = 7;
            return ctx.db.collection('user').findOne({ 'userlogin': userInfo.userlogin, 'userPassword': userInfo.userPassword });

          case 7:
            userData = _context.sent;

            taskData.userId = userData._id;
            _context.next = 11;
            return ctx.db.collection('tasks').insertOne(taskData);

          case 11:
            db = _context.sent;

            ctx.body = db.ops;
            _context.next = 19;
            break;

          case 15:
            _context.prev = 15;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);
            ctx.status = _context.t0.statusCode || _context.t0.status || 500;

          case 19:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 15]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();