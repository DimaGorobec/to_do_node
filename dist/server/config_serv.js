'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _koaRouter = require('koa-router');

var _koaRouter2 = _interopRequireDefault(_koaRouter);

var _post_task = require('./tasks_metods/post_task');

var _post_task2 = _interopRequireDefault(_post_task);

var _getAllUSer = require('./auth/getAllUSer');

var _getAllUSer2 = _interopRequireDefault(_getAllUSer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// const getAllUser = require('./auth/getAllUSer')
// const Router = require('koa-router');


// const { ObjectID } = require('mongodb').ObjectID;
// const postTask = require('./tasks_metods/post_task');
// const changeStatus = require('./tasks_metods/change_status');
// const deleteTask = require('./tasks_metods/delete_task')
// const ChangeText = require('./tasks_metods/change_text')
// const createUser = require('./auth/create_user')
// const searchUserToToken = require('./auth/getUser')
// const login = require('./auth/userLogin_server')
// const getId = require('./auth/get_id_user')
// const getAllUser = require('./auth/getAllUSer')
// const postAccess = require('./auth/openAccess')
// const getAccesUser = require('./auth/getAccessUser')

function configRouter() {
  var router = new _koaRouter2.default({
    prefix: '/task'
  });
  // router.post('/', async (ctx) => {
  //   try {
  //     const userData = await getId.getIdUser(ctx)
  //     const { userId } = userData
  //     const { newToken } = userData
  //     const userTasks = await ctx.db.collection('tasks').find({ userId }).toArray()
  //     ctx.body = { userTasks, newToken }
  //     ctx.status = 200
  //   } catch (err) {
  //     console.error(err)
  //     ctx.status = err.statusCode || err.status || 500;
  //   }
  // })
  // router.post('/accesUser/', getAccesUser.getAccesUser)
  // router.post('/create_user/', createUser.createUser)
  // router.post('/searchUser/', searchUserToToken.getUserToToken)
  // router.post('/createTask/', postTask())
  // router.post('/login/', login.userLogin)
  router.post('/getAllUser/', _getAllUSer2.default);
  // router.put('/change_status/:id', changeStatus.changeStatus)
  // router.delete('/:id', deleteTask.deleteTask)
  // router.put('/change_text/:id', ChangeText.ChangeText)
  // router.post('/openAccess/', postAccess.openAccessViewing)
  return [router.routes(), router.allowedMethods()];
}

exports.default = configRouter;